#!/bin/bash
set -e
source .env.build

if ! [[ -d ../logs/apache ]]; then
    mkdir -p ../logs/apache
fi

if ! [[ -d ../logs/mysql ]]; then
    mkdir -p ../logs/mysql
fi

if ! [[ -d ../logs/php ]]; then
    mkdir -p ../logs/php
fi

if [[ -d $GIT_DOWNLOAD ]]; then
    rm -rf $GIT_DOWNLOAD
fi

if [[ -d $GIT_TAR ]]; then
    rm -rf $GIT_TAR
fi

mkdir -p $GIT_TAR
mkdir -p $GIT_DOWNLOAD



