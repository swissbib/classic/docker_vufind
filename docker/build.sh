#!/bin/bash
set -e

source .env.build
source ./initialize.sh


git clone https://github.com/swissbib/vufind.git $GIT_DOWNLOAD
cd $GIT_DOWNLOAD
git archive --format=tar  --o=$GIT_TAR/deploycode.tar master

cd ${GIT_TAR}
tar xf deploycode.tar
# Lösche überflüssigen Code
rm ${GIT_TAR}/deploycode.tar
rm  ${GIT_TAR}/import-* ${GIT_TAR}/index* ${GIT_TAR}/install*
rm -rf ${GIT_TAR}/harvest/ ${GIT_TAR}/import/ ${GIT_TAR}/sbDocumentation ${GIT_TAR}/solr/
composer install --no-dev

cd $GIT_TAR
npm-install-cache
npm run build


cd $BASE

#docker-compose up -d --build
#docker-compose build

#docker exec helloworld_apache_con chown -R root:www-data /usr/local/apache2/logs
#docker exec helloworld_php_con chown -R root:www-data /usr/local/etc/logs